# Gasha WAF (Web Application Firewall) Documentation

## Overview

Gasha WAF is a comprehensive Web Application Firewall solution designed to provide robust security for your web applications. This package includes various components such as the WAF engine, antivirus module, core rule set, and a log aggregator. The following documentation provides a detailed guide on preparing, installing, updating, and uninstalling the Gasha WAF package.

## Contents

1. **nginx-waf.deb**: The compiled Debian package of the Gasha WAF Engine, ready for installation.
2. **Gasha-WAF-nginx**: The source code for the WAF Engine, including modules for WAF, antivirus, core ruleset, and log aggregation.
3. **dependency.sh**: A script that installs all necessary dependencies for the Gasha WAF.
4. **WAF-service-dev**: Files for the WAF updating and upgrading service, including the `waf_updater` binary.


## Installation Guide

### Prerequisites

Ensure that your system meets the following requirements:
- Ubuntu 20.04 or later
- Sufficient privileges to install packages (sudo access)

### Steps to Install

1. **Download the Package**

   Download the `nginx-waf.deb` file to your system.

2. **Install Dependencies**

   Run the `dependency.sh` script to install all necessary dependencies:
   ```sh
   sudo ./dependency.sh
   ```
3. **Install the Gasha WAF Package

   Navigate to the directory containing nginx-waf.deb and run:

   ```sh   
    sudo dpkg -i nginx-waf.deb
   ```
   Follow the prompts to enter your username, password, and activation key.

## Updating API Endpoint
   If you need to update the IP or domain name of the API endpoint, follow these steps:

### Steps to Update API Endpoint

1. Update API References

   Update the API URLs inside the following files:

   - `preinst` and `postinst` scripts in Gasha-WAF-nginx/DEBIAN
   - `waf_update.py` inside WAF-service-dev/

2. Rebuild the waf_updater Binary

   Navigate to the WAF-service-dev directory and run:

   ```sh
   pyinstaller -n "waf_updater" --noconsole -F waf_update.py
   ```
3. Replace the Old waf_updater Binary

   Copy the newly built waf_updater from WAF-service-dev/dist/waf_updater to Gasha-WAF-nginx/usr/local/bin/:

   ```sh
   cp WAF-service-dev/dist/waf_updater Gasha-WAF-nginx/usr/local/bin/
   ```
4. Rebuild the Debian Package

   Navigate to the src directory and rebuild the package:

   ```sh
   cd src
   dpkg-deb --build Gasha-WAF-nginx
   ```
5. Install/Reinstall the Updated Package

   Install the newly built Debian package:

   ```sh
   sudo dpkg -i Gasha-WAF-nginx.deb
   ```

## Uninstallation Guide
  To uninstall the Gasha WAF, you have several options:

### Standard Uninstallation
   ```sh
    sudo dpkg --remove waf-nginx
   ```
### Purge Uninstallation
   ```sh
    sudo apt-get purge waf-nginx
   ```
### Forcing Removal
    If the package is in a broken state and cannot be removed using the above methods, force the removal:

   ```sh
    sudo dpkg --purge --force-all waf-nginx
    sudo apt-get clean
    sudo apt-get update
   ```

## Detailed File Structure
  ```
Gasha-WAF-nginx/
├── DEBIAN
│   ├── control
│   ├── preinst
│   ├── postinst
│   ├── prerm
│   └── postrm
├── etc
│   ├── logstash
│   │   ├── bin
│   │   ├── config
│   │   ├── CONTRIBUTORS
│   │   ├── data
│   │   ├── Gemfile
│   │   ├── Gemfile.lock
│   │   ├── jdk
│   │   └── ...
│   ├── nginx
│   │   ├── clamav_script
│   │   └── modsec
│   │       ├── coreruleset-3.3.4
│   │       │   ├── rules
│   │       │   │   ├── REQUEST-901-INITIALIZATION.conf
│   │       │   │   ├── REQUEST-920-PROTOCOL-ENFORCEMENT.conf
│   │       │   │   └── ...
│   │       ├── main.conf
│   │       ├── modsecurity.conf
│   │       └── unicode.mapping
│   ├── wafUpdater
│   │   └── gasha_config.ini
├── usr
│   ├── lib
│   │   └── systemd
│   │       └── system
│   │           ├── clamd.service
│   │           ├── freshclam.service
│   │           ├── logstash.service
│   │           ├── upload_monitor.service
│   │           └── wafUpdater.service
│   ├── local
│   │   ├── bin
│   │   ├── etc
│   │   ├── include
│   │   ├── lib
│   │   ├── modsecurity
│   │   ├── sbin
│   │   └── share
│   └── share
│       └── nginx
│           └── modules
│               ├── ngx_http_modsecurity_module.so

```

## Frequently Asked Questions (FAQ)
### What do I do if I encounter a dpkg lock error?
If you encounter a dpkg lock error, it means another package management process is running. You can either wait for it to finish or manually kill the process using:

   ```sh
    sudo killall dpkg
   ```
### How can I verify if Gasha WAF is installed correctly?
You can verify the installation by checking the status of the services and testing the nginx configuration:

   ```sh
    sudo systemctl status nginx
    sudo systemctl status logstash
    sudo nginx -t
   ```
### How do I update the WAF rules?
To update the WAF rules, modify the rules files located in Gasha-WAF-nginx/etc/nginx/modsec/coreruleset-3.3.4/rules/. After updating the rules, restart nginx:

   ```sh
   sudo systemctl restart nginx
   ```
### Who do I contact for support?
For any issues or support, contact the Gasha WAF support team at support@gasha-waf.com.

# Conclusion
The Gasha WAF provides a robust and comprehensive solution for securing your web applications. By following this guide, you can ensure proper installation, configuration, and maintenance of the WAF on your systems. For any additional questions or issues, refer to the FAQ section or contact support.